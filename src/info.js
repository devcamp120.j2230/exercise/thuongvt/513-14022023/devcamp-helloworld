export const gDevcampReact = {
    title: 'Chào mừng đến với Devcamp React',
    image: 'https://reactjs.org/logo-og.png',
    benefits: ['Blazing Fast', 'Seo Friendly', 'Reusability', 'Easy Testing', 'Dom Virtuality', 'Efficient Debugging'],
    studyingStudents: 20,
    totalStudents: 100
  }

   //Hàm trả về các sinh viên đang theo học
  export function countPercentStudyingStudent (){
    return gDevcampReact.studyingStudents/gDevcampReact.totalStudents *100
  }