
import { gDevcampReact,countPercentStudyingStudent } from "./info";
import image from "./assets/images/logo-og.png"

function App() {
  var tyLeSv = countPercentStudyingStudent()
  return (
    <div>
    <h1>{gDevcampReact.title}</h1>
    <img src={gDevcampReact.image} alt="devcamp" width="500"/>
    <p>Tỷ lệ sinh viên đang theo học: {tyLeSv} %</p>
    <p>{tyLeSv >=15 ?"Sinh viên đăng ký theo học nhiều":"Sinh viên theo học ít"}</p>
    <ul>
      {gDevcampReact.benefits.map((value,index)=>{
        return <li key={index}>
          {value}
        </li>
      })}
    </ul>
    </div>
  );
}

export default App;